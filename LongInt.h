//
// Created by Irina Vergunova on 2019-10-13.
//

#ifndef LONGINT_LONGINT_H
#define LONGINT_LONGINT_H

#include <ostream>
#include <vector>

using namespace std;

//class Mult {};
class LongInt {
private:

   int a;
   string b;
public:
    //static Mult *m;
    //static Mult& setMult(const Mult &m);
    LongInt();
    LongInt(string a);
    LongInt(int b);
    vector<int> Digits;
    LongInt operator+(const LongInt &other) const;
    LongInt operator-(const LongInt &other) const;
    LongInt operator*(const LongInt &other) const;
    friend ostream& operator<<(ostream &os, const LongInt &other);
    LongInt Karatsuba(const LongInt& a,const LongInt& b);
    bool Ferma(const LongInt&a,int k);
    bool MillerRabin(const LongInt&a,int k);

};







//class Multi : public LongInt{};
//long long mul(long long a, long long b, long long m);
// long long gcd(long long a, long long b);
//long long pows(long long a, long long b, long long m);
#endif //LONGINT_LONGINT_H
