//
// Created by Irina Vergunova on 2019-10-13.
//

#include "LongInt.h"
#include <cmath>
#include <iostream>
LongInt::LongInt() {
    Digits.push_back(0);
}

LongInt::LongInt(string a) {
     this->a=stoi(a);
     int aInt= stoi(a);
     int sizeaInt= a.size();
     int iterSizeaInt=sizeaInt;
    for(int iter = 0; iter<iterSizeaInt;++iter){
        int power=pow(10,sizeaInt-1);
        int b=aInt/power;
        Digits.push_back(b);
        aInt= aInt%power;
        sizeaInt--;
    }
}
LongInt::LongInt(int b) {
    this->a=b;
    int len = to_string(b).length();
    int iterSizeaInt=len;
    for(int iter = 0; iter<iterSizeaInt;++iter){
        int power=pow(10,len-1);
        int v=b/power;
        Digits.push_back(v);
        b= b%power;
        len--;
    }
}
LongInt LongInt::operator+(const LongInt &other) const {
   LongInt sum;
   sum.a=a+other.a;
   return sum;
}
LongInt LongInt::operator-(const LongInt &other) const {
    LongInt minus;
    minus.a=a-other.a;
    return minus;
}
LongInt LongInt::operator*(const LongInt &other) const {
    LongInt mult;
    mult.a=a *other.a;
    return  mult;
}
ostream& operator<<(std::ostream &os, const LongInt &other){
    os<<other.a<<endl;
    return os;
}
LongInt LongInt::Karatsuba(const LongInt &a, const LongInt &b) {
    int n=0;
    LongInt prod1,prod2,prod3;
    int len1=to_string(a.a).length();
    int len2=to_string(b.a).length();
    n=max(len1,len2);
    if (n==1)
        return a*b;
    else {
         int N=n/2;
         string X_l="",X_r="",Y_l="",Y_r="";
         int smthX_l=0,smthX_r=0,smthY_l=0,smthY_r=0;
         int intX_l=0, intX_r=0,intY_l=0, intY_r=0;
         for(int i=0; i<N;++i) {
           smthX_l = a.Digits[N-1-i];
           X_l=to_string(smthX_l)+X_l;
         }
         intX_l=stoi(X_l);
         int iter=a.Digits.size()-1;
         while (iter>N-1){
             smthX_r=a.Digits[iter];
             X_r=to_string(smthX_r)+X_r;
             iter--;
         }
         intX_r=stoi(X_r);
        for(int i=0; i<N;++i) {
            smthY_l = b.Digits[N-1-i];
            Y_l=to_string(smthY_l)+Y_l;
        }
        intY_l=stoi(Y_l);
        int iter1=b.Digits.size()-1;
        while (iter1>N-1){
            smthY_r=b.Digits[iter1];
            Y_r=to_string(smthY_r)+Y_r;
            iter1--;
        }
        intY_r=stoi(Y_r);
        LongInt obj1(intX_l), obj2(intX_r), obj3(intY_l), obj4(intY_r);
        prod1=Karatsuba(obj1,obj3);
        prod2=Karatsuba(obj2,obj4);
        prod3=Karatsuba(obj1+obj2,obj3+obj4);
       return prod1*pow(10,n)+(prod3-prod1-prod2)*pow(10,N)+prod2;
    }
}
bool LongInt::Ferma(const LongInt &a, int k) {
    string  objA=this->b;
    if(objA<to_string(3)) {
       // cout<< "Pick bigger number for cheking";
        return true;
    }
    else{
        for(int i=0; i<k;++i) {
            int powe = 0, mod = 0;
           // int r = rand() % (objA - 1) + 2;
           // powe = pow(r, objA - 1);
            //mod = powe % objA;
            int a =2;
            mod=int(pow(9,10))%11;
            if(mod!=1) {
                cout << "composite" << endl;
                return false;
            }
        }
        return true;
    }
}
//11 2*5+1
bool LongInt::MillerRabin(const LongInt &a, int k) {
    int exA=this->a;
    if (exA<=3) return true;
    int n=exA-1;
    int r=0;
        while(n%2==0){
        n/=2;
        r+=1;
        }
        int i=0;
     while(i<k){
         int a=rand()%(exA-2)+2;
         int power=pow(a,n);
         int x=power%exA;
         if(x==1 || x==n)
             continue;
         while(i<r-1){
             x=(x*x) % exA;
             if(x==n)
                 break;
         }
         return false;
     }
     return true;

}
















/*bool LongInt::Ferma(long long x){
    if(x == 2)
        return true; srand(time(NULL));
    for(int i=0;i<100;i++){
        long long a = (rand() % (x - 2)) + 2;
        if (gcd(a, x) != 1)
            return false;
        if( pows(a, x-1, x) != 1)
            return false;
    }
    return true;
}
long long LongInt::gcd(long long a, long long b){
    if(b==0)
        return a;
    return gcd(b, a%b);
}
long long LongInt::mul(long long a, long long b, long long m){
    if(b==1)
        return a;
    if(b%2==0){
        long long t = mul(a, b/2, m);
        return (2 * t) % m;
    }
    return (mul(a, b-1, m) + a) % m;
}

long long LongInt::pows(long long a, long long b, long long m){
    if(b==0)
        return 1;
    if(b%2==0){
        long long t = pows(a, b/2, m);
        return mul(t , t, m) % m;
    }
    return ( mul(pows(a, b-1, m) , a, m)) % m;
}
 */